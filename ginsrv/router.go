package ginsrv

import (
	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	"gitlab.com/espressiotech/core2go"
	"net/http"
)

// GetDefaultRouter creates new gin router with default middlewares
func GetDefaultRouter() *gin.Engine {
	router := gin.New()

	// Recover panics with formatted log
	router.Use(gin.CustomRecovery(func(c *gin.Context, recovered interface{}) {
		ctx := NewContextHandler(c)
		log.Error(recovered)
		if s, ok := recovered.(string); ok {
			ctx.ErrS(s, http.StatusInternalServerError)
		}
		ctx.AbortWithStatus(http.StatusInternalServerError)
	}))

	// JSON-formatted logs
	router.Use(gin.LoggerWithFormatter(M().LogFormatter))
	router.Use(M().LogBody())

	// Init language from header
	router.Use(M().InitI18nLang())

	// 8 Mb limit for uploads
	router.MaxMultipartMemory = 8 << 20

	// Custom no-route error
	router.NoRoute(func(ctx *gin.Context) {
		ctx.JSON(
			http.StatusNotFound,
			core2go.NewError(http.StatusNotFound, "unknown endpoint"),
		)
	})

	return router
}
