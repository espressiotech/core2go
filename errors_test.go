package core2go_test

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/espressiotech/core2go"
	"net/http"
	"testing"
)

func TestNewError(t *testing.T) {
	err := core2go.NewError(400, "test")
	assert.Error(t, err)
	assert.Equal(t, 400, err.Code)
	assert.Equal(t, "test", err.Message)

	err = core2go.NewError(http.StatusNotFound)
	assert.Error(t, err)
	assert.Equal(t, 404, err.Code)
	assert.Equal(t, "Not Found", err.Message)
}

func TestError_Error(t *testing.T) {
	errs := map[string]*core2go.Error{
		"test1": core2go.NewError(1, "test1"),
		"test2": core2go.NewError(2, "test2"),
		"test3": core2go.NewError(2, "test", 3),
	}

	for text, err := range errs {
		assert.Equal(t, text, err.Error())
	}
}
