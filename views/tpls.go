package views

import (
	"github.com/gin-contrib/multitemplate"
	"text/template"
)

// Tpls is a templates handler
type Tpls struct {
	renderer   multitemplate.Renderer
	standalone map[string]*template.Template
}

// GetRenderer returns initialized multi-template renderer instance
func (t *Tpls) GetRenderer() multitemplate.Renderer {
	if t.renderer == nil {
		t.renderer = multitemplate.NewRenderer()
	}

	return t.renderer
}

// GetStandaloneTemplate returns standalone template instance
func (t *Tpls) GetStandaloneTemplate(name string) (tpl *template.Template, err error) {
	t.initStandalone()

	tpl, ok := t.standalone[name]
	if !ok {
		return nil, ErrTplNotFound
	}

	return tpl, nil
}

// AddStandaloneTemplate register new standalone template
func (t *Tpls) AddStandaloneTemplate(name string, tpl *template.Template) {
	t.initStandalone()
	t.standalone[name] = tpl
}

func (t *Tpls) initStandalone() {
	if t.standalone == nil {
		t.standalone = map[string]*template.Template{}
	}
}
