package views

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/espressiotech/core2go"
	"os"
)

// NewViewData creates new ViewData instance
func NewViewData() *ViewData {
	return &ViewData{
		AppTitle:       core2go.Opt.AppTitle,
		Data:           gin.H{},
		staticVersions: map[string]string{},
	}
}

// ViewData is a data passed to views
type ViewData struct {
	// AppTitle is a human-readable app title
	AppTitle string

	// Title is a page title
	Title string

	// DoNotTrack is a value of the DNT header
	DoNotTrack bool

	// Data is a custom key-value data
	Data gin.H

	staticVersions map[string]string
}

// GetTitle returns page title
func (v *ViewData) GetTitle() string {
	title := ""
	if v.Title != "" {
		title = v.Title + " — "
	}

	title += v.AppTitle

	return title
}

// GetAssetURL returns asset file URL
func (v *ViewData) GetAssetURL(name string) string {
	url := core2go.Opt.ViewsAssetsURL + "/" + name
	url += "?v=" + v.getStaticFileVersion(name)

	return url
}

// GetAssetURLWithHost returns asset file URL with leading host
func (v *ViewData) GetAssetURLWithHost(name string) string {
	return core2go.Opt.ViewsAssetsHost + v.GetAssetURL(name)
}

func (v *ViewData) getStaticFileVersion(name string) string {
	path := core2go.Opt.ViewsAssetsPath + "/" + name

	version, ok := v.staticVersions[path]
	if ok {
		return version
	}

	file, err := os.Stat(path)
	version = core2go.Opt.InstanceToken

	if err == nil {
		version = file.ModTime().Format("20060102150405")
	}

	v.staticVersions[path] = version

	return version
}
