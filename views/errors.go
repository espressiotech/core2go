package views

import (
	"gitlab.com/espressiotech/core2go"
	"net/http"
)

// Views errors
var (
	// ErrTplNotFound is a template not found error
	ErrTplNotFound = core2go.NewError(http.StatusInternalServerError, "err.tpls.notfound")
)
