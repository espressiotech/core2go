package app

import (
	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"gitlab.com/espressiotech/core2go"
	"testing"
)

func TestConfig_SetFromViper(t *testing.T) {
	core2go.Opt.ConfigFilePath = "../.test.env"
	v, err := core2go.ReadConfig()
	if !assert.NoError(t, err) {
		return
	}

	conf := &Config{}
	conf.SetFromViper(v)

	assert.Equal(t, "80", conf.ServicePort)

	assert.Equal(t, "localhost:6379", conf.RedisHost)
	assert.Equal(t, "localhost:27017", conf.MongoHost)

	conf.ApplyToGlobals()

	assert.Equal(t, []byte("12345"), core2go.Opt.JWTPassword)
	assert.Equal(t, log.InfoLevel, log.GetLevel())
}
