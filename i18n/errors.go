package i18n

import (
	"gitlab.com/espressiotech/core2go"
	"net/http"
)

// ErrInvalidLang is a Language validation error
var ErrInvalidLang = core2go.NewError(http.StatusBadRequest, "invalid language code given")
