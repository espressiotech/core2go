module gitlab.com/espressiotech/core2go

go 1.16

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-contrib/multitemplate v0.0.0-20211002122701-e9e3201b87a0
	github.com/gin-gonic/gin v1.7.7
	github.com/go-redis/redis/v8 v8.11.0
	github.com/google/uuid v1.2.0
	github.com/json-iterator/go v1.1.11
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/viper v1.8.0
	github.com/stretchr/testify v1.7.0
	go.mongodb.org/mongo-driver v1.5.3
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b
)
