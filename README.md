## Espressio services core

Shared core for the Espressio services.

### Installation

```shell
git clone https://gitlab.com/espressiotech/espcore.git
cd core2go
make
```

Also, you can use `make build_fast` to skip some tests & linters while build.