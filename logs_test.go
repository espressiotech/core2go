package core2go_test

import (
	log "github.com/sirupsen/logrus"
	"github.com/stretchr/testify/assert"
	"gitlab.com/espressiotech/core2go"
	"testing"
)

func TestInitLogs(t *testing.T) {
	core2go.Opt.LogLevelDft = log.WarnLevel
	core2go.InitLogs()
	assert.Equal(t, log.WarnLevel, log.GetLevel())
	log.SetLevel(log.DebugLevel)
}
