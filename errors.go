package core2go

import (
	"fmt"
	"net/http"
)

// Errors
var (
	ErrUserBlocked      = NewError(http.StatusForbidden, "err.user.blocked")
	ErrPassFailed       = NewError(http.StatusUnauthorized, "err.user.password")
	ErrRoleNotAllowed   = NewError(http.StatusForbidden, "err.user.unexpectedrole")
	ErrTokenInvalid     = NewError(http.StatusUnauthorized, "err.token.invalid")
	ErrTokenExpired     = NewError(http.StatusUnauthorized, "err.token.expired")
	ErrTokenUnsupported = NewError(http.StatusUnprocessableEntity, "err.token.unsupportedsignmethod")
	ErrNotFound         = NewError(http.StatusNotFound, "err.notfound")
)

// NewError creates a new Error instance
func NewError(code int, messages ...interface{}) *Error {
	if len(messages) == 0 && code >= 100 && code <= 599 {
		messages = []interface{}{
			http.StatusText(code),
		}
	}
	return &Error{
		Code:    code,
		Message: fmt.Sprint(messages...),
	}
}

// Error defines the response error
type Error struct {
	Code      int    `json:"code" example:"403"`
	Message   string `json:"message" example:"err.notfound"`
	Localized string `json:"localized,omitempty" example:"Not Found"`
}

// Error as a string
func (e *Error) Error() string {
	return e.Message
}
