package core2go_test

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/espressiotech/core2go"
	"os"
	"testing"
)

func TestGetServiceInfo(t *testing.T) {
	info := core2go.GetServiceInfo()
	assert.Equal(t, core2go.Opt.APIVersion, info.Version)
	assert.Equal(t, core2go.Opt.BasePath, info.BasePath)
}

func TestOptions_GetHostname(t *testing.T) {
	opt := &core2go.Options{
		Hostname: "test_hostname",
	}
	assert.Equal(t, "test_hostname", opt.GetHostname())

	osHost, _ := os.Hostname()

	opt = &core2go.Options{}
	assert.Equal(t, osHost, opt.GetHostname())
}

func TestReadConfig(t *testing.T) {
	core2go.Opt.ConfigFilePath = ".test.env"
	core2go.Opt.ConfigFileType = "env"

	conf, err := core2go.ReadConfig()
	if !assert.NoError(t, err) {
		return
	}

	assert.Equal(t, "test1", conf.GetString("test_env_var"))
	assert.Equal(t, "test2", conf.GetString("OTHER_TEST_ENV_VAR"))

	core2go.Opt.ConfigFilePath = "__unkown_file__.env"
	_, err = core2go.ReadConfig()
	assert.Error(t, err)
}
